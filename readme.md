About
=====

pqtt is designed to be small, lightweight and fast mqtt client. It is posix
complaint, so it can be run on Linux as well as on microcontrolers that
follows posix standards.
